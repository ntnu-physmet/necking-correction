This directory contains procesed data for a paper, where the necking corrections are applied.

Feng Lu, Jonas K. Sunde, Calin D. Marioara, Randi Holmestad, Bjørn Holmedal. An improved modelling framework for strength and work hardening of precipitate strengthened Al–Mg–Si alloys.Materials Science & Engineering A 832 (2022) 142500
https://doi.org/10.1016/j.msea.2021.142500
