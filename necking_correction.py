import time
starttime = time.time()
import os; import glob ; import io
from tkinter import * 
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
import numpy as np
import pandas as pd
from scipy.interpolate import UnivariateSpline
from scipy import signal
from scipy.optimize import curve_fit
import cv2 
import PIL as PIL
import PIL.Image
import PIL.ImageTk
from matplotlib import pyplot as plt
import operator

def SlctFolder():  
    # This function prompts user to select the working directory that contains images and txt log file
    # Will be called with clicking the button "Select Directory" 
    dirname = filedialog.askdirectory(parent=root,initialdir="/",title='Please select a directory')
    if dirname:
        global files ; files={}         
        files['txt']= glob.glob(dirname+ "/*.txt")   # A list of URL of all txt files in directory
        files['png0']= glob.glob(dirname+ "/*.png")   # A list of URL of all png files in directory
        #  verifying that there is more than 10 images and only one text file in the specified directory
        if len(files['png0'])>=10:
            if len(files['txt'])==1:              
                # Making output directory named "Logs" if it already doesn't exist
                global OutputDir ; OutputDir=dirname + "/Logs"
                if not os.path.exists(OutputDir):
                    os.makedirs(OutputDir)   
                PreAnalysis()          
            else:
                messagebox.showinfo("Error", "Error in reading text file (Either none or multiple text files in the directory) ")
                statustext.set('Status: Error');  root.update()  
        else:
            messagebox.showinfo("Error", "Error in reading image files (less than 10 images) ")
            statustext.set('Status: Error');  root.update()  

def UpdateButton():
    # Starts Processing the data in the directory
    # Will be called with clicking the button "Update"
    PreAnalysis() 
    # To Read Force, Displacemet data from the log file,
    # finds Necking point, makes Alpha list, plots load vs time    
    Process1()    
    
def PreAnalysis(): 
    ax1.cla()   # Clearing present subplot in the graph tab figure
    progress['value']=0

    # reading text file columns     
    global log ; log={}  
    files['png']=files['png0']      
    txt_ = pd.read_csv(files['txt'][0],delimiter = "\s+",engine = "python",names=["Frame","Force","Displacement",'a','b'])     
    log['L']=txt_["Frame"].tolist()
    log['F']=txt_["Force"].tolist() ; log['D']=txt_["Displacement"].tolist()   
    # Removing initial rows
    for i in range(len(log['L'])):
        if log['L'][i]=='1' and log['L'][i+1]=='2' and log['L'][i+2]=='3':
            Cuti=i; break    
    log['L']=log['L'][Cuti:] ; log['F']=log['F'][Cuti:] ; log['D']=log['D'][Cuti:]
    # Making time list based on the given "measuring time step"
    log['T']=(np.arange(0,len(log['L'])*Mstep.get(),Mstep.get())).tolist()    
       

    # Making sure image and data lists are of the same size, if not, cutting extra from either image or data list
    global TNO ; TNO=min(len(log['F']),len(files['png']))
    files['png'] = files['png'][:TNO]    
    log['L'] = log['L'][:TNO]  ; log['T'] = log['T'][:TNO]  
    log['F'] = log['F'][:TNO]  ; log['D'] = log['D'][:TNO]   
    # Showing in the screen total number of records  
    TotalImgVar.set('Total number of records:  %d' % TNO)
            
    # Selecting range defined by User
    I=SkipI.get() ; F=SkipF.get() # skipping I first elements and F Last elements
    del files['png'][TNO-F:] ; del files['png'][:I]
    for key in log.keys():
        del log[key][TNO-F:] ; del log[key][:I]            

    # Selecting every X item only
    files['png'] = every_X(files['png'],everyXimage.get())
    for key in log.keys():  log[key] = every_X(log[key],everyXimage.get())     
    
    global RNO; RNO=len(log['L']) # Total Number of Records
    ProcessImgVar.set('Number of images to process:  %d' %RNO)    
    
    # Converting string values to float
    log['F']=[float(xx) for xx in log['F'] ]; log['D']=[float(xx) for xx in log['D'] ]; log['L']=[int(xx) for xx in log['L'] ]
    
    # Showing first and last images in the Crop boxes, and justifying aspect ratio of boxes according to the image
    cimg=[]                
    cimg.append(cv2.imread(files['png'][0])) ;cimg.append(cv2.imread(files['png'][RNO-1]))
    global PicDim;  PicDim['H'], PicDim['W'], no_channels = cimg[0].shape                   
    global Crop_zs  ; Crop_zs = CropCanvas_W/PicDim['W']     # Crop Pic Resizing scale    
    cimg = [cv2.resize(xx, (0,0), fx=Crop_zs , fy=Crop_zs ) for xx in cimg] # Resizing image to fit the crop box
    
    ImgOrigSize.set('  %d   x   %d  ' %(PicDim['W'],PicDim['H']))    
    global Crp
    Crp={'W':ImgCropW.get(),'H':ImgCropH.get() , 'X':ImgCropX.get(),'Y':ImgCropY.get(),'M':ImgCropM.get() } # get Crop Dimentions
    
    global img_crop ; img_crop=[]  
    for i,cvs in enumerate(CropCanvas):
        img_crop.append(PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cimg[i]))) # Converting CV2 image to tkinter Canvas type using PIL, and putting in a list
        cvs.config(height=CropCanvas_W*PicDim['H']/PicDim['W'])  # modifying canas aspect ratio                   
        cvs.create_image(0, 0, image=img_crop[i],anchor=N+W)  
        # showing default crop Rectangle
        pi=1 if i==0 else 0
        clr='blue' if i==0 else 'yellow'
        CropCanvas[i].delete(CropCanvas[i].find_withtag('CropRec'))
        CropCanvas[i].create_rectangle(Crop_zs*(-(pi*Crp['M'])+Crp['X']), Crop_zs*Crp['Y'], Crop_zs*(-(pi*Crp['M'])+Crp['X']+Crp['W']), Crop_zs*(Crp['Y']+Crp['H']), outline=clr,tags='CropRec')   
    
    FindNeck() # finds Necking point, makes Alpha list, plots load vs time
    
    # Making Crop Movement list
    log['M']=(np.linspace(Crp['M'],0,RNO)).tolist() ; log['M']=[int(xx) for xx in log['M'] ]
    
    # Calcualting Resize Scale for images in Param Tab and Results Tab
    global Param_zs ; 
    if ParamPic['W']/ParamPic['H']<Crp['W']/Crp['H']:
        Param_zs = ParamPic['W']/Crp['W'] ;  ParamPic['X0']=0 ; ParamPic['Y0']=(ParamPic['H']-Param_zs*Crp['H'])/2
    else:
        Param_zs = ParamPic['H']/Crp['H'] ;  ParamPic['X0']=(ParamPic['W']-Param_zs*Crp['W'])/2  ; ParamPic['Y0']=0
    
    global Con_zs ; 
    if ConPic['W']/ConPic['H']<Crp['W']/Crp['H']:
        Con_zs = ConPic['W']/Crp['W'] ;  ConPic['X0']=0 ; ConPic['Y0']=(ConPic['H']-Con_zs*Crp['H'])/2
    else:
        Con_zs = ConPic['H']/Crp['H'] ;  ConPic['X0']=(ConPic['W']-Con_zs*Crp['W'])/2  ; ConPic['Y0']=0
        
    statustext.set('Status: \nData selected')
    progress['value']=10
    root.update()  
    
    global iW ; global iH ; global iWLen ; global iHLen ;
    iH = np.arange(0,Crp['H'],dtype=int) # 0 to Height array
    iW = np.arange(0,Crp['W'],1,dtype=int) # 0 to Width array 
    iWLen=len(iW) ; iHLen=len(iH)  
    
    
def FindNeck():
    # Plots Load vs Time graph, and marks necking point
    # Smoothing only for better finding the Necking point
    log['SF'] = UnivariateSpline(log['T'], log['F'], k=3,s =SmDegree.get())(log['T']).tolist()    
    global NeckIndex ; NeckIndex = log['SF'].index(max(log['SF'][10:-10]))   # Finding the position of Necking
    
    ax1.set_title('Load vs Time') ;  ax1.set_xlabel('time [s]') ;   ax1.set_ylabel('load [KN]')      
    ax1.plot( log['T'], log['F'] ,linewidth=1,color='blue')
    ax1.plot( log['T'], log['SF'] ,linewidth=1,color='red')  
    
    Make_Aplphalist()    
    pts=ALPHAlist     
    for ptsx in pts:
        ax1.plot(log['T'][ptsx],log['F'][ptsx],"o",color = 'green')      
    ax1.plot( [log['T'][NeckIndex],log['T'][NeckIndex]], [log['F'][NeckIndex],0],linewidth=1,linestyle='-.',color='green')    
    ax1.grid()
    
    # putting figure in Canvas
    global fig_photo
    fig_photo=FigtoImg(f1,Graph_CVS)     
    Graph_CVS.create_image(0, 0, image=fig_photo, anchor=N+W)

def Make_Aplphalist():
    A = 0
    #B = 1*NeckIndex//3
    C = 2*NeckIndex//3
    #D = NeckIndex
    #E = NeckIndex+1*(RNO-NeckIndex)//6
    F = NeckIndex+2*(RNO-NeckIndex)//6
    #G = NeckIndex+3*(RNO-NeckIndex)//6
    H = NeckIndex+4*(RNO-NeckIndex)//6
    #I = NeckIndex+5*(RNO-NeckIndex)//6
    J = NeckIndex+11*(RNO-NeckIndex)//12
    #K = RNO-1
    global ALPHAlist 
    ALPHAlist = [A,C,F,H,J]    
    
def FigtoImg(xfig,canvas):
    figDim= xfig.get_size_inches()*xfig.dpi  # Figure dimensions 
    buf = io.BytesIO()
    xfig.savefig(buf, format='png') ; buf.seek(0)    
    figimg0=cv2.resize(np.array(PIL.Image.open(buf)), (0,0), fx=canvas.winfo_width()/figDim[0] , fy=canvas.winfo_height()/figDim[1] ) # Resizing graph image
    return PIL.ImageTk.PhotoImage(image= PIL.Image.fromarray(figimg0))  # Converting CV2 image to tkinter Canvas type using PIL

def Process1():                
    global img_Param ; img_Param=[]    # List of images to be shown in Parameters Tab Canvases, needs to be stored global otherwise dissapears!
    global hist_Param ; hist_Param=[]  # List of graphs to be shown in Parameters Tab Canvases, needs to be stored global otherwise dissapears!     
    global Pimg ; Pimg=[]    # To store original size cropped images with contours, needed For Zoom
    global Pimg0; Pimg0=[]   # To store original size cropped images without contours, needed For Zoom
    global Ptime ; Ptime=[]  # for time of capture be shown on image

    line_Pos =Line_PosVar.get() * Crp['W']//100  # Scale line position
    # thickness of contour line
    thk=Line_thkVar.get()-1   
    global thk_list; thk_list=[] ; thk_list.append(0)
    for t in range(-1*thk,thk+1):  
        if t!=0: thk_list.append(t)
        
    for ni,i in enumerate(ALPHAlist):
        img_url = files['png'][i]
        the_img = cv2.imread(img_url, cv2.IMREAD_COLOR) # Reading Image at selected position from list ([A,C,F,H,J)    
        the_img = the_img[Crp['Y']:Crp['Y']+Crp['H'],Crp['X']-log['M'][i]:Crp['X']-log['M'][i]+Crp['W'] ]  # Croping the image 
        Pimg0.append(the_img.copy()) 
        # cv2.imwrite(OutputDir+"/"+img_url[-8:-4]+"_tr.png",the_img)       # Saving cropped image
        # transfering from color to grayscale + smoothing (noise canceling)        
        blurdegree = BlurDegVar.get()+1  # this parameter means x*x pixels are put into consideration, must be increased for more noise canceling
        ToProcess_img=cv2.medianBlur(cv2.cvtColor(the_img, cv2.COLOR_BGR2GRAY),blurdegree) 
        # finding contours and putting them in data_set  
        data_set = np.transpose(EdgeDetect(ToProcess_img))            
        frontpoint_U = data_set[0]   ; frontpoint_L = data_set[1]   # Yellow 
        endpoint_U =  data_set[2]    ; endpoint_L = data_set[3]     # Magenta
        contour_U = data_set[4]      ; contour_L =data_set[5]       # red   
        ret_index_U = data_set[6]  ; ret_index_L = data_set[7]  # green
        #put everything into image to make sure contour is good   
        
        the_img[:,line_Pos-4:line_Pos+4] = [0,255,255] # Cyan   , Scan line
        for t in thk_list:              
            clr= [255,0,0] # Red 
            the_img[contour_U+t,iW] =clr ; the_img[contour_L+t,iW] =clr              
            clr= [0,255,0]  # green
            the_img[ret_index_U+t,iW] =clr ; the_img[ret_index_L+t,iW] =clr             
            clr= [255,0,255] # Magenta
            the_img[endpoint_U+t,iW] =clr ; the_img[endpoint_L+t,iW] =clr               
            clr= [255,255,0] # yellow 
            the_img[frontpoint_U+t,iW] =clr ; the_img[frontpoint_L+t,iW] =clr               

            # Storing NOT Scaled Images with countours, line thickness=1 in a list, needed for zooming                
            if t==0: Pimg.append(the_img.copy())                

        # Storing Scaled Images with countours, line thickness=t in another list    
        Pimg_scaled = cv2.resize(the_img, (0,0), fx=Param_zs, fy=Param_zs)
        img_Param.append(PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(Pimg_scaled))) # Converting CV2 image to tkinter Canvas type using PIL, and putting in a list   
        Ptime.append(log['T'][i]) 

        # Gray Scale level at line_Pos, with different specific points
        Col_Pos=ToProcess_img[:,line_Pos]
        plt.figure(figsize=(4,8))
        plt.plot(Col_Pos,iH ,linewidth=0.5, color = "black")  
        for ci in range(0,8):
            if ci==0 or ci==1: clrt="yellow"
            if ci==2 or ci==3: clrt="Magenta"
            if ci==4 or ci==5: clrt="red"
            if ci==6 or ci==7: clrt="green"     
            plt.plot(Col_Pos[data_set[ci][line_Pos]],data_set[ci][line_Pos],"o",color = clrt)            
        
        plt.grid() ; f2=plt.gcf()
        f2.savefig(OutputDir+"/"+img_url[-8:-4] + "_hist.png",figsize=(16,9), dpi = 600)         
        hist_Param.append(FigtoImg(f2,ParamCanvas[0])) 
        plt.close()  

        if ni==0 or ni==1:            
            # getting rotation and pixel size scaling parameters by using least square fit, linear euqaiton slope will be used for calculating the rotation angle    
            WX = np.array([iW, np.ones(Crp['W'])]).T # X Values matrix
            a1, b1 = np.linalg.lstsq(WX, contour_U,rcond=None)[0]    # Upper contour slop and y-intercept
            a2, b2 = np.linalg.lstsq(WX, contour_L,rcond=None)[0]    # Lower contour slop and y-intercept 
            am=(a1+a2)/2 ; bm=(b1+b2)/2                              # Bisector line slope and y-intercept 
            ix1=0 ;       iy1=am*ix1+bm  # Bisector line start point
            ix2=iWLen-1 ; iy2=am*ix2+bm  # Bisector line end  point
            
            if ni==0:
                global pixel_diameter ; global pixel_per_mm 
                # must find perpendeculars to bisector line y=am*x+bm >> y=am_p*x+bm_p and average it
                if am!=0: #bisector isn't horizontal
                    am_p=-1/am  # slope of Perpendecular line
                    bm_p=iy1-am_p*ix1 # y-intercept of Perpendecular line, passiong through start point
                    Dist1=Intersect_Dist(am_p,bm_p,a1,b1,a2,b2)  
                    bm_p=iy2-am_p*ix2  # y-intercept of Perpendecular line, passiong through end point
                    Dist2=Intersect_Dist(am_p,bm_p,a1,b1,a2,b2)
                    Av_Dist=(Dist2+Dist1)/2                        
                else:  #bisector ishorizontal
                    Av_Dist=(Dist2Points(ix1,ix1,b1,b2)+Dist2Points(ix2,ix2,ix2*a1+b1,ix2*a2+b2))/2                   
                pixel_diameter=Av_Dist
                pixel_per_mm = pixel_diameter/SpecDiaVar.get()    
                #print("pixel per mm =",pixel_per_mm,"pixels in diameter =", pixel_diameter)
                pixel_lenVar.set('%.2f' %pixel_per_mm)
                statustext.set('Status: Pixel length Calculated');  root.update()  
            if ni==1:
                global rotate_angle; rotate_angle = np.arctan(am) # in Radians
                rotate_angle_deg=np.rad2deg(rotate_angle) ; a1_deg=np.rad2deg(np.arctan(a1)) ; a2_deg=np.rad2deg(np.arctan(a2))
                Rotation_AngleVar.set('Average:          %.6f \nBottom Edge: %.6f \nTop Edge:       %.6f' %(rotate_angle_deg,a1_deg,a2_deg))
                #Midpoint on Bisector line
                global Pivot ; Pivot=[]; Pivot.append((ix1+ix2)/2) ; Pivot.append((iy1+iy2)/2)   
                statustext.set('Status: Rotaion Angle Calculated');  root.update()  


            fig, (ax1, ax2) = plt.subplots(2, 1)
            ax1.plot(iW, contour_U, 'ro',  marker="o",  markersize=1, color = "red", label='lower contour')
            ax1.plot(iW, a1*iW + b1,linewidth=1, color = "blue", label='lower fitline')
            ax2.plot(iW, contour_L, 'ro',  marker="o",  markersize=1, color = "red", label='upper contour')
            ax2.plot(iW, a2*iW + b2,linewidth=1, color = "blue", label='upper fitline')
            ax1.grid(True) ; ax1.legend() ;  ax2.grid(True) ; ax2.legend()
            plt.savefig(OutputDir+"/"+img_url[-8:-4]+"_fit.png",figsize=(16,9),dpi=600)
            plt.close()  
        
        progress['value']=progress['value']+22.5
        root.update()
        
    ImageOrHist()        
    statustext.set('Status: \nParameters Calculated')
    root.update() 

def Intersect_Dist(a0,b0,a1,b1,a2,b2):
    x1=(b0-b1)/(a1-a0)
    y1=a0*x1+b0
    x2=(b0-b2)/(a2-a0)
    y2=a0*x2+b0     
    return Dist2Points(x1,x2,y1,y2)
def Dist2Points(x1,x2,y1,y2):
    return np.sqrt((y2-y1)**2+(x2-x1)**2)
    
    
def EdgeDetect(img):
    M1 = M1Var.get()   # default=2/10 , endpoint (background, close to edge) position factor, smaller means closer to center. 
    M2 = M2Var.get()   # default=20 ,  front point (background, far from edge) position factor, smaller means closer to center.    
    data_set = []    
        
    for Imgcol in img.T:   
        max_value = max(Imgcol) # the max grey scale in the pixel line
        ret0,th = cv2.threshold(Imgcol,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        ret = ret0*retVar.get() #0.744
        Mposition = np.where(Imgcol == max_value) # the position of the max grey scale, will be several probably
        max_index1 = Mposition[0][0]              # first max grey scale position in this pixel line, for upper contour
        max_index2 = Mposition[0][-1]             # last max grey scale position in this pixel line,  for lower contour
        
        # upper portion analysis     
        ret_index_U = np.where(Imgcol[:max_index1] >= ret)[0][0] # first position in the upper half of pixel line with grey scale larger than ret 
        Ta = (Imgcol[ret_index_U]-Imgcol[ret_index_U-1])
        Tb = Imgcol[ret_index_U] -(Ta)*ret_index_U
        Tmaxindex_U = int((ret-Tb)/Ta)
        
        # lower portion analysis    
        ret_index_L = max_index2 + np.where(Imgcol[max_index2:] >= ret)[0][-1] # last position in the lower half of pixel line with grey scale larger than ret 
        Ta2 = -1*(Imgcol[ret_index_L]-Imgcol[ret_index_L + 1])
        Tb2 = Imgcol[ret_index_L] -(Ta2)*ret_index_L
        Tmaxindex_L = int((ret-Tb2)/Ta2)
        
        # distance between two grey scale=ret pixels
        UL_distance = abs(Tmaxindex_L-Tmaxindex_U)
        
        # endpoint: a point that belongs to the background   
        epU0 = int(round(Tmaxindex_U - M1*UL_distance/2))
        epL0 = int(round(Tmaxindex_L + M1*UL_distance/2))          
        # frontpoint
        thk=Line_thkVar.get()-1   #0-5        
        fpU0 = int(round(epU0 - UL_distance)) 
        if fpU0-thk < 0 : fpU0 = thk                            # fpU0 should not be less than contour half line thinkness
        fpL0 = int(round(epL0 + UL_distance))
        if fpL0+thk > len(Imgcol)-1 : fpL0 = len(Imgcol)-2-thk  # fpL0 should not be greater than contour half line thinkness + image height   
        
        upper_contour_l = Imgcol[epU0:max_index1] 
        upper_contour_l_points = np.arange(epU0,max_index1,1)
        upper_line = Imgcol[fpU0:epU0] # upper_line is from the frontpoint to endpoint, this is a list contains grey scale of the background for determine the upper contour
        upper_line_points = np.arange(fpU0,epU0,1)       
        WX = np.array([upper_line_points, np.ones(epU0-fpU0)]).T # X Values matrix             
        a1, b1 = np.linalg.lstsq(WX, upper_line,rcond = None)[0] # slope and constant of the linear equation fiting the chosed upper line
        
        lower_contour_l = Imgcol[max_index2:epL0]
        lower_contour_l_points = np.arange(max_index2,epL0,1)
        lower_line = Imgcol[epL0:fpL0]
        lower_line_points = np.arange(epL0,fpL0,1)        
        WX = np.array([lower_line_points, np.ones(fpL0-epL0)]).T   
        a2, b2 = np.linalg.lstsq(WX, lower_line,rcond = None)[0] 

        standard_d1 = np.std(upper_line, ddof=1) # standard deviation of the Upper background        
        standard_d2 = np.std(lower_line, ddof=1) # standard deviation of the Lower background     
        new_b1 = standard_d1*M2 + b1          # standard deviation * coefficent + b1 = new liear eqation constant
        new_b2 = standard_d2*M2 + b2      

        fx = upper_contour_l_points*a1 + new_b1*np.ones(len(upper_contour_l_points)) # use the linear equation to calcuate the potiental point, 
        minus_value1 = np.abs(upper_contour_l - fx) # if it is closr to the value of the grey scale, it should be contour
        potantial_con1_position1 = np.where(minus_value1 == min(minus_value1)) # get the position of the contour
        contour_U= epU0+potantial_con1_position1[0][-1]
        
        fx = lower_contour_l_points*a2 + new_b2*np.ones(len(lower_contour_l_points)) # use the linear equation to calcuate the potiental point, 
        minus_value2 = np.abs(lower_contour_l - fx) # if it is closr to the value of the grey scale, it should be contour
        potantial_conl_position2 = np.where(minus_value2 == min(minus_value2)) # get the position of the contour
        contour_L= max_index2+(potantial_conl_position2[0][0])          
        
        data_set.append([fpU0,fpL0,epU0,epL0,contour_U,contour_L,Tmaxindex_U,Tmaxindex_L])       

    return data_set
                    
def ImageOrHist():
    if radio_Imgorhis.get()=='image':  
        for ni,i in enumerate(ParamCanvas):
            ParamCanvas[ni].delete("all")              
            ParamCanvas[ni].create_image(ParamPic['X0'], ParamPic['Y0'], image=img_Param[ni],anchor=NW)   
            ParamCanvas[ni].create_text(30,ParamPic['H']-10,fill="red",font=('verdana', 7),text="t=%.1f s" %Ptime[ni])
        Update_Zoombox(True) 
    if radio_Imgorhis.get()=='hist':  
        for ni,i in enumerate(ParamCanvas):
            ParamCanvas[ni].delete("all")        
            ParamCanvas[ni].create_image(0, 0, image=hist_Param[ni], anchor=NW) 
        Zbox.delete("all")   
   
def choose_contour (m_x,min_dist,factor1,factor2): 
    left = int(round(m_x-min_dist*factor1)) 
    right = int(round(m_x+min_dist*factor2))
    if left <= 0:        left = 1
    if right >= iWLen:   right = iWLen-1
    return left, right
    
def Run(): 
    PreAnalysis
    tb.select([3])  
    
    tme=time.time()
    sample_diameters =[] 
    up_rads = []  ;   lo_rads = []
    up_aRs = []  ;  lo_aRs = []   
    E_Eu=[]    
    KCB1s=[] ; KCB2s=[]
    
    progress['value']=0   
    statustext.set('Status: \nAnalysing Images...')  ; root.update()
    
    # Getting ranges (Hleft,Hright) based on point H    
    contour_U , contour_L = GetContours(ALPHAlist[3]) # point H
    y1 = signal.medfilt(contour_U,medfiltpara)
    y2 = signal.medfilt(contour_L,medfiltpara)
    BH = (ALPHAlist[3]-NeckIndex)/(RNO-NeckIndex) ; AH = (RNO-ALPHAlist[3])/(RNO-NeckIndex) # Range
    m_x,min_y1,min_y2,min_dist = diameter_finder(y1,y2,iWR)
    global Hleft ; global Hright
    Hleft, Hright= choose_contour (m_x,min_dist,BH/2,AH/2)     
    
    # Minimum Diameter right at the necking point
    contour_U , contour_L = GetContours(NeckIndex) 
    global Ny1 ; global Ny2 
    Ny1 = signal.medfilt(contour_U,medfiltpara)
    Ny2 = signal.medfilt(contour_L,medfiltpara)
    Nminimal_p_x = iWR[Hleft:Hright]
    Nminimal_p_y1 = Ny1[Hleft:Hright]
    Nminimal_p_y2 = Ny2[Hleft:Hright]
    global Nm_x ; global Nm_y1 ; global Nm_y2 ; global Ncoarse_dmin
    Nm_x,Nm_y1,Nm_y2,Ncoarse_dmin = diameter_finder(Nminimal_p_y1,Nminimal_p_y2,Nminimal_p_x) 
          
    sample_diameter0=0
    for i in range(RNO):          
        dia , up_rad, lo_rad,up_aR,lo_aR, KCB1, KCB2 = contourfiting(i,sample_diameter0)
        if i==0: sample_diameter0=dia
        sample_diameters.append(dia/pixel_per_mm)        
        up_rads.append(up_rad/pixel_per_mm)
        lo_rads.append(lo_rad/pixel_per_mm)            
        up_aRs.append(up_aR) ; lo_aRs.append(lo_aR)        
        KCB1s.append(KCB1) ;  KCB2s.append(KCB2)
        
        AreaArr=[np.pi*np.square(sd/2) for sd in sample_diameters]
        uncor_stress = np.divide(log['F'][:i+1], AreaArr)*1000 # uncorrected true stress    
        true_strain = 2*np.log((sample_diameters[0]*np.ones(len(sample_diameters)))/sample_diameters)
        if i>NeckIndex: E_Eu=true_strain-true_strain[NeckIndex]*np.ones(len(true_strain))  

        #print('diameter,diameter2,R1,R2',i,dia1/pixel_per_mm,dia2/pixel_per_mm,up_rad/pixel_per_mm,lo_rad/pixel_per_mm)        
        if i>NeckIndex:
            # plotting a/R(L,R)
            ax1.cla()
            ax1.set_title('a/R') ;  ax1.set_xlabel('E-Eu []') ;   ax1.set_ylabel('a/R')     
            ax1.plot( E_Eu[NeckIndex:], up_aRs[NeckIndex:] ,linewidth=1,color='blue',label='a/R(L)')
            ax1.plot( E_Eu[NeckIndex:], lo_aRs[NeckIndex:] ,linewidth=1,color='red',label='a/R(R)')
            ax1.grid(True) ; ax1.legend()
            global aR_photo ; aR_photo=FigtoImg(f1,aR_CVS)     
            aR_CVS.create_image(0, 0, image=aR_photo, anchor=N+W)      
       
        # plotting Minimum Diameter
        ax4.cla()    
        ax4.set_xlabel('True Strain') ;   ax4.set_ylabel('Minimum Diameter [mm]')     
        ax4.plot( true_strain, sample_diameters ,linewidth=1,color='blue')
        ax4.grid(True) 
        global minD_photo ;  minD_photo=FigtoImg(f4,minD_CVS)     
        minD_CVS.create_image(0, 0, image=minD_photo, anchor=N+W)
        
        # plotting engineering and true stress curves
        ax4.cla()    
        ax4.set_xlabel('time [s]') ;   ax4.set_ylabel('Stress [MPa]')           
        Area0Arr=np.ones(len(log['F']))*(np.pi*np.square(SpecDiaVar.get()/2))
        ES=np.divide(log['F'],Area0Arr)*1000
        ax4.plot( log['T'], ES  ,linewidth=1,color='blue',label='Engineering Stress')
        ax4.plot( log['T'][:i+1], uncor_stress ,linewidth=1,color='green',label='Uncorrecred True Stress')
        ax4.plot( log['T'][i], ES[i] ,"o",color='red')
        ax4.grid(True) ; ax4.legend()
        global RunP_photo ;  RunP_photo=FigtoImg(f4,RunP_CVS)     
        RunP_CVS.create_image(0, 0, image=RunP_photo, anchor=N+W)
        
        
        statustext.set('Status: \n %d of %d Images Analysed' %(i+1,RNO))
        progress['value']=100*(i+1)/RNO
        root.update()          
    
    f1.savefig(OutputDir+"/aR.png",figsize=(13,8), dpi = 600)    
    f4.savefig(OutputDir+"/Stress.png",figsize=(9,6), dpi = 600)       
   
    #Some Values don't have meaning for index<Neckindex, putting them equal to 0 for that range:
    DL=NeckIndex+1
    up_rads[:DL]=[0]*DL ; lo_rads[:DL]=[0]*DL
    up_aRs[:DL]=[0]*DL ; lo_aRs[:DL]=[0]*DL ; E_Eu[:DL]=[0]*DL

    # exporting data to an excel file
    d = {"time (s)": log['T'][:RNO],"Force(kN)": log['F'][:RNO], "KCB1s":KCB1s, "KCB2s":KCB2s,
         "Diameter(mm)" : sample_diameters, "Radius (L,mm)":up_rads ,"Radius (R,mm)" : lo_rads, "a/R(L)": up_aRs, 
         "a/R(R)": lo_aRs, "Uncorrected Stress(Mpa)":uncor_stress,"True Strain(E)":true_strain, "E-Eu":E_Eu, "necking_number":NeckIndex}
    df = pd.DataFrame(data=d, index=None)
    with pd.ExcelWriter(OutputDir+"/Results.xlsx") as writer:
        df.to_excel(writer, sheet_name='sheet0')
    
    print('time to run:',time.time()-tme)  
    statustext.set('Status: Done')
    progress['value']=100
    root.update()

def contourfiting(i,sample_diameter0):      
    if i < NeckIndex:
        sample_diameter,up_R,lo_R,up_aR,lo_aR, KCB1, KCB2 = flat_fitting(i)
        #up_R,lo_R,up_aR, lo_aR, coarse_dmins1 ,ff_dmins1,sample_diameter,m_x,m_y1,m_y2,\
        #rootupx,rootupy,rootlox, rootloy,frootupx,frootupy,frootlox, frootloy \
        #= flat_fitting(i)                       
    else:  
        sample_diameter,up_R,lo_R,up_aR,lo_aR, KCB1, KCB2 = arch_fitting(i,sample_diameter0) 
        #up_R,lo_R,up_aR, lo_aR, coarse_dmins2 ,ff_dmins2,sample_diameter,m_x,m_y1,m_y2,\
        #rootupx,rootupy,rootlox, rootloy,frootupx,frootupy,frootlox, frootloy,\
        #KCB1, KCB2,aR_up, aR_lo, aR_up1, aR_lo1 = arch_fitting(i,sample_diameter0)   
    
    return sample_diameter,up_R,lo_R,up_aR,lo_aR, KCB1, KCB2 

def flat_fitting(i):
    order=2        
    contour_U , contour_L = GetContours(i)  # Getting Rotated Contours for each Image
    y1 = signal.medfilt(contour_U,medfiltpara)
    y2 = signal.medfilt(contour_L,medfiltpara)
    minimal_p_x = iWR[Hleft:Hright]
    minimal_p_y1 = y1[Hleft:Hright]
    minimal_p_y2 = y2[Hleft:Hright]
    m_x,m_y1,m_y2,coarse_dmin = diameter_finder(minimal_p_y1,minimal_p_y2,minimal_p_x)      
    
    left11,right11  = choose_contour (m_x,coarse_dmin,0.5,0.5)
    nar_c_x = iWR[left11:right11]
    nar_c_y1 = y1[left11:right11]
    nar_c_y2 = y2[left11:right11]
    R_lo, rootlox, rootloy, yfitlo = fitting_func(nar_c_x, nar_c_y1,order)
    R_up, rootupx, rootupy, yfitup = fitting_func(nar_c_x, nar_c_y2,order)    
    ff_dmin = rootupy - rootloy
    
    f_up_left,f_up_right = choose_contour (rootupx,ff_dmin,0.5,0.5)
    f_lo_left,f_lo_right = choose_contour (rootlox,ff_dmin,0.5,0.5)

    nar_fs_x2= iWR[f_up_left:f_up_right]
    nar_fs_x1= iWR[f_lo_left:f_lo_right]
    nar_fs_y2= y2[f_up_left:f_up_right]
    nar_fs_y1= y1[f_lo_left:f_lo_right]
    fR_lo, frootlox, frootloy, fyfitlo = fitting_func(nar_fs_x1, nar_fs_y1,order)
    fR_up, frootupx, frootupy, fyfitup = fitting_func(nar_fs_x2, nar_fs_y2,order)
  
    fR_lo = abs(fR_lo) if fR_lo < 0 else 1000000
    fR_up = fR_up if fR_up > 0 else 1000000    
    
    fR_up=min(fR_up,1000000) ; fR_lo=min(fR_lo,1000000)  
    
    sf_dmin = frootupy - frootloy       
    aR_upp = sf_dmin/fR_up/2
    ar_loo = sf_dmin/fR_lo/2    
    '''
    #UP (2) >> 
    X1 = plt.Circle((frootupx,frootupy+fR_up),fR_up,color="purple",fill=False)
    ax5 = plt.gca() ;  ax5.cla() 
    ax5.plot(iWR,y2, color = 'green')                   # Contour line
    ax5.plot(nar_c_x,yfitup,color = 'orange')
    ax5.plot(nar_fs_x2,fyfitup,color = 'red')           # Fitted Chebyshev polynomial
    ax5.plot(rootupx,rootupy,".",color = 'blue')        # Extermum point of polynomial
    ax5.plot(frootupx,frootupy,".",color = 'black')     # 
    ax5.plot(m_x,m_y2,".",color = 'yellow')            # Neck point suggested by interpolation
    ax5.add_artist(X1) ;   ax5.grid(True)     
    global fitL_photo ;  fitL_photo=FigtoImg(f5,fitL_CVS)     
    fitL_CVS.create_image(0, 0, image=fitL_photo, anchor=N+W)
    # Lo (1) >>    
    X2 = plt.Circle((frootlox,frootloy-fR_lo),fR_lo,color="purple",fill=False)
    ax5 = plt.gca() ;   ax5.cla() 
    ax5.plot(iWR,y1, color = 'green')
    ax5.plot(nar_c_x,yfitlo,color = 'orange')
    ax5.plot(nar_fs_x1,fyfitlo,color = 'red')
    ax5.plot(rootlox,rootloy,".",color = 'blue')
    ax5.plot(frootlox,frootloy,".",color = 'black')
    ax5.plot(m_x,m_y1,".",color = 'yellow')
    ax5.add_artist(X2) ;  ax5.grid(True)     
    global fitU_photo ;  fitU_photo=FigtoImg(f5,fitU_CVS)     
    fitU_CVS.create_image(0, 0, image=fitU_photo, anchor=N+W) 
    '''
    #return fR_up,fR_lo, aR_upp, ar_loo, coarse_dmin ,ff_dmin,sf_dmin,m_x,m_y1,m_y2, \
    #rootupx,rootupy,rootlox, rootloy,frootupx,frootupy,frootlox, frootloy
    return sf_dmin,fR_up,fR_lo, aR_upp, ar_loo, 1, 1

def arch_fitting(i,d_inital):
    order=2        
    contour_U , contour_L = GetContours(i)  # Getting Rotated Contours for each Image
    y1 = signal.medfilt(contour_U,medfiltpara)
    y2 = signal.medfilt(contour_L,medfiltpara)
    minimal_p_x = iWR[Hleft:Hright]
    minimal_p_y1 = y1[Hleft:Hright]
    minimal_p_y2 = y2[Hleft:Hright]
    m_x,m_y1,m_y2,coarse_dmin = diameter_finder(minimal_p_y1,minimal_p_y2,minimal_p_x) 
    
    true_strain_coarse = abs(2*np.log((Ncoarse_dmin)/coarse_dmin))    
    true_strain_uniform =  abs(2*np.log((d_inital)/Ncoarse_dmin))         
    Factor1 = necking_factor(true_strain_uniform,true_strain_coarse)     
    
    left11,right11  = choose_contour (m_x,coarse_dmin,Factor1,Factor1)
    nar_c_x = iWR[left11:right11]
    nar_c_y1 = y1[left11:right11]
    nar_c_y2 = y2[left11:right11]
    R_lo, rootlox, rootloy, yfitlo = fitting_func(nar_c_x, nar_c_y1,order)
    R_up, rootupx, rootupy, yfitup = fitting_func(nar_c_x, nar_c_y2,order)    
    ff_dmin = rootupy - rootloy
        
    left110,right110  = choose_contour (Nm_x ,Ncoarse_dmin,Factor1,Factor1)    
    nar_c_xa0 = iWR[left110:right110]    
    nar_c_y1a0 = Ny1[left110:right110]
    nar_c_y2a0 = Ny2[left110:right110]    
    R_loa0, rootloxa0, rootloya0, yfitloa0 = fitting_func(nar_c_xa0, nar_c_y1a0,order)
    R_upa0, rootupxa0, rootupya0, yfitupa0 = fitting_func(nar_c_xa0, nar_c_y2a0,order)
    ff_dmina0 = rootupya0 - rootloya0          

    true_strain_ff = abs(2*np.log((ff_dmina0)/ff_dmin))
    true_strain_uniform_ff =  abs(2*np.log((d_inital)/ff_dmina0)) 
    Factor2 = necking_factor(true_strain_uniform_ff,true_strain_ff) 
    
    f_up_left,f_up_right = choose_contour (rootupx,ff_dmin,Factor2,Factor2)
    f_lo_left,f_lo_right = choose_contour (rootlox,ff_dmin,Factor2,Factor2)
    nar_fs_x2= iWR[f_up_left:f_up_right]
    nar_fs_x1= iWR[f_lo_left:f_lo_right]
    nar_fs_y2= y2[f_up_left:f_up_right]
    nar_fs_y1= y1[f_lo_left:f_lo_right]
    fR_lo, frootlox, frootloy, fyfitlo = fitting_func(nar_fs_x1, nar_fs_y1,order)
    fR_up, frootupx, frootupy, fyfitup = fitting_func(nar_fs_x2, nar_fs_y2,order)    
    
    f_up_left1,f_up_right1 = choose_contour (frootupx,ff_dmin,Factor2*2,Factor2*2)
    f_lo_left1,f_lo_right1 = choose_contour (frootlox,ff_dmin,Factor2*2,Factor2*2)
    nar_ft_x2= iWR[f_up_left1:f_up_right1]
    nar_ft_x1= iWR[f_lo_left1:f_lo_right1]
    nar_ft_y2= y2[f_up_left1:f_up_right1]
    nar_ft_y1= y1[f_lo_left1:f_lo_right1]
    fR_up1, frootupx1, frootupy1, fyfitup1 = fitting_func(nar_ft_x2, nar_ft_y2,order)
    fR_lo1, frootlox1, frootloy1, fyfitlo1 = fitting_func(nar_ft_x1, nar_ft_y1,order)
    
    fR_lo = abs(fR_lo) if fR_lo < 0 else 1000000
    fR_up = fR_up if fR_up > 0 else 1000000    
    fR_lo1 = abs(fR_lo1) if fR_lo1 < 0 else 1000000
    fR_up1 = fR_up1 if fR_up1 > 0 else 1000000        
    fR_up=min(fR_up,1000000) ; fR_lo=min(fR_lo,1000000) 
    fR_up1=min(fR_up1,1000000) ; fR_lo1=min(fR_lo1,1000000)           
   
    sf_dmin = frootupy - frootloy    
    L  = sf_dmin*Factor2*2
    L2 = sf_dmin*Factor2*2*2
    x_data = [L**2,L2**2]
    y_data_up = [sf_dmin/2/fR_up,sf_dmin/2/fR_up1]
    y_data_lo = [sf_dmin/2/fR_lo,sf_dmin/2/fR_lo1]    
    
    popt_up, pcov_up = curve_fit(linearfit, x_data, y_data_up,method= "lm")
    popt_lo, pcov_lo = curve_fit(linearfit, x_data, y_data_lo,method= "lm")
    aRRR_up = popt_up[1] ; aRRR_lo = popt_lo[1]
    if sf_dmin/2/fR_up <= aRRR_up <= 1.15* sf_dmin/2/fR_up:
        aR_upp = aRRR_up
    else:
        aR_upp = sf_dmin/2/fR_up1      
    
    if sf_dmin/2/fR_lo <= aRRR_lo <= 1.15* sf_dmin/2/fR_lo:
        aR_loo = aRRR_lo
    else:
        aR_loo = sf_dmin/2/fR_lo1
        
    aR_up = sf_dmin/2/fR_up     ;    aR_lo = sf_dmin/2/fR_lo    
    aR_up1 = sf_dmin/2/fR_up1   ;    aR_lo1 = sf_dmin/2/fR_lo1
        
    #UP (2) >> 
    X1 = plt.Circle((frootupx,frootupy+fR_up),fR_up,color="purple",fill=False)
    ax5 = plt.gca()  ;   ax5.cla() 
    ax5.plot(iWR,y2, color = 'green')                   # Contour line
    ax5.plot(nar_c_x,yfitup,color = 'orange')
    ax5.plot(nar_fs_x2,fyfitup,color = 'red')           # Fitted Chebyshev polynomial
    ax5.plot(rootupx,rootupy,".",color = 'blue')        # Extermum point of polynomial
    ax5.plot(frootupx,frootupy,".",color = 'black')     # 
    ax5.plot(m_x,m_y2,".",color = 'yellow')            # Neck point suggested by interpolation
    ax5.add_artist(X1)  ;  ax5.set_ylim(ax5.get_ylim()[::-1]) ;   ax5.grid(True) 
    global fitL_photo ;  fitL_photo=FigtoImg(f5,fitL_CVS)     
    fitL_CVS.create_image(0, 0, image=fitL_photo, anchor=N+W)
    # Lo (1) >>    
    X2 = plt.Circle((frootlox,frootloy-fR_lo),fR_lo,color="purple",fill=False)
    ax5 = plt.gca()
    ax5.cla() 
    ax5.plot(iWR,y1, color = 'green')
    ax5.plot(nar_c_x,yfitlo,color = 'orange')
    ax5.plot(nar_fs_x1,fyfitlo,color = 'red')
    ax5.plot(rootlox,rootloy,".",color = 'blue')
    ax5.plot(frootlox,frootloy,".",color = 'black')
    ax5.plot(m_x,m_y1,".",color = 'yellow')
    ax5.add_artist(X2)  ;  ax5.set_ylim(ax5.get_ylim()[::-1]) ;  ax5.grid(True) 
    global fitU_photo ;  fitU_photo=FigtoImg(f5,fitU_CVS)     
    fitU_CVS.create_image(0, 0, image=fitU_photo, anchor=N+W) 
    
    #return fR_up,fR_lo, aR_upp, aR_loo, coarse_dmin ,ff_dmin,sf_dmin,m_x,m_y1,m_y2, \
    #rootupx,rootupy,rootlox, rootloy,frootupx,frootupy,frootlox, frootloy,\
    #Factor1, Factor2, aR_up, aR_lo, aR_up1, aR_lo1
    return sf_dmin,fR_up,fR_lo, aR_upp, aR_loo, Factor1, Factor2

def linearfit(x,a,b):
        return x*a+b
    
def diameter_finder(y1,y2,x):
    Delta = list(map(operator.sub, y2, y1))
    MinDelta=min(Delta)    
    MinDelta_poses = np.where(Delta == MinDelta )[0]   
    n = int(len(MinDelta_poses)/2)  
    m_x = x[MinDelta_poses[n]] 
    min_y1 = y1[MinDelta_poses[n]] 
    min_y2 = y2[MinDelta_poses[n]] 
    return m_x,min_y1,min_y2,MinDelta

def necking_factor(strain_u,neckingstrain):
    if neckingstrain == 0:
        factor = 0.5
    else:
        x = neckingstrain          
        factor = min(2, (0.877*(1/x)**(1/10) + 0.153*(x)**2-0.535)*1.8)*0.25
    return factor

def fitting_func(nar_c_x1, nar_c_y1, order):    
    C = np.polynomial.chebyshev.chebfit(nar_c_x1, nar_c_y1, deg= order, rcond=None, full=False, w=None)
    yfit1 = np.polynomial.chebyshev.chebval(nar_c_x1, C, tensor=True)
    diff2 = np.polynomial.chebyshev.chebder(C, m=2, scl=1, axis=0)
    diff1 = np.polynomial.chebyshev.chebder(C, m=1, scl=1, axis=0)
    roots1 = np.polynomial.chebyshev.chebroots(diff1)
    ic = roots1[0]    
    if nar_c_x1[0] < ic < nar_c_x1[-1] :
        f_x1 = ic
    elif  ic >= nar_c_x1[-1]:
        f_x1 = nar_c_x1[-1]
    elif  ic <= nar_c_x1[0]:
        f_x1 = nar_c_x1[0]
    f_y1 =  np.polynomial.chebyshev.chebval(f_x1, C, tensor=True) 
    y_dprime = np.polynomial.chebyshev.chebval(f_x1, diff2, tensor=True)
    R = 1./(y_dprime)
    return R, f_x1, f_y1, yfit1
        

def GetContours(i):
    # gets index of the image and return the rotated upper and lower contours
    img_url = files['png'][i]
    the_img = cv2.imread(img_url, cv2.IMREAD_COLOR) # Reading Image number i     
    the_img = the_img[Crp['Y']:Crp['Y']+Crp['H'],Crp['X']-log['M'][i]:Crp['X']-log['M'][i]+Crp['W'] ]  # Croping the image   
    # transferring from color to grayscale + smoothing (noise canceling)        
    blurdegree = BlurDegVar.get()+1  # this parameter means x*x pixels are put into consideration, must be increased for more noise canceling
    ToProcess_img=cv2.medianBlur(cv2.cvtColor(the_img, cv2.COLOR_BGR2GRAY),blurdegree)     
    # finding contours and putting them in data_set
    data_set = np.transpose(EdgeDetect(ToProcess_img))    
    contour_U= data_set[4] ; contour_L=data_set[5]     
    
    # Showing image with contours in Run Tab    
    for t in thk_list:              
            clr= [255,0,0] # Red 
            the_img[contour_U+t,iW] =clr ; the_img[contour_L+t,iW] =clr     
    Con_img_scaled = cv2.resize(the_img, (0,0), fx=Con_zs, fy=Con_zs) # Resizing the image
    global Con_img ; Con_img =PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(Con_img_scaled)) # Converting CV2 image to tkinter Canvas type using PIL, and storing it
    Con_CVS.create_image(ConPic['X0'], ConPic['Y0'], image=Con_img,anchor=NW)   # putting it in Canvas
                
    # Rotating Contours to Straighten them
    contour_U=Rotate2D(contour_U) ;  contour_L=Rotate2D(contour_L)
    return  contour_U  ,  contour_L    
        
def Rotate2D(y):
    '''Rotates list of points (x,y) about center pivot by angle ang in radian and returns rotated x,y'''
    # Rotation angle and Pivot are defined before using Point C
    ang=-1*rotate_angle
    pts= np.column_stack((iW,y))
    rslt=np.dot(pts-Pivot,np.array([[np.cos(ang),np.sin(ang)],[-np.sin(ang),np.cos(ang)]]))+Pivot
    global iWR ; iWR=rslt[:,0]
    return rslt[:,1]    

def Update_Zoombox(HasContour): 
    global Pimg ; global Pimg0 
    zbsize=int(100/ZoomVar.get())
    bw2=zbsize/2 ; bh2=(bw2*2/ZboxRatio)/2 ; w=ParamPic['W'] ; h=ParamPic['H']       
    if  Zoom_xyn[0]==0: # First load         
        Zoom_xyn[0]=ParamPic['W']/2  ;  Zoom_xyn[1]=ParamPic['H']/2 ; Zoom_xyn[2]=0
    x=Zoom_xyn[0] ; y=Zoom_xyn[1] ; n=Zoom_xyn[2]
    if x-bw2<ParamPic['X0']: x=bw2+ParamPic['X0']
    if y-bh2<ParamPic['Y0']: y=bh2+ParamPic['Y0']
    if x+bw2>w-ParamPic['X0']: x=w-bw2-ParamPic['X0']
    if y+bh2>h-ParamPic['Y0']: y=h-bh2-ParamPic['Y0']      
    for i in range(0,4+1):
        ParamCanvas[i].delete('ZoomRec')        
    ParamCanvas[n].create_rectangle(x-bw2,y-bh2 , x+bw2,y+bh2, outline="yellow",tags='ZoomRec')  
    x=x-ParamPic['X0'] ; y=y-ParamPic['Y0']    
    r1=int((x-bw2)/Param_zs) ;  r2=int((x+bw2)/Param_zs) ; c1=int((y-bh2)/Param_zs) ;  c2=int((y+bh2)/Param_zs)   
    global ZoomBox_zs ; ZoomBox_zs =ParamPic['W']*4/(2*bw2/Param_zs)     # Resizing scale 
    if HasContour==True: 
        zoomimg=cv2.resize(Pimg[n][c1:c2,r1:r2], (0,0), fx=ZoomBox_zs, fy=ZoomBox_zs)      
    else:
        zoomimg=cv2.resize(Pimg0[n][c1:c2,r1:r2], (0,0), fx=ZoomBox_zs, fy=ZoomBox_zs) 
    global Zboximg 
    Zboximg=PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(zoomimg))
    Zbox.create_image(0, 0, image=Zboximg,anchor=NW) 
    
def Update_Zoomlevel(event):
    if radio_Imgorhis.get()=='image' and started==True: 
        Update_Zoombox(True)
    
def Param_button_press(event):    
    if radio_Imgorhis.get()=='image': 
        widgetno=str(event.widget)[-1:]
        if widgetno=='s': widgetno='1' 
        Zoom_xyn[0]=event.x ;  Zoom_xyn[1]=event.y ; Zoom_xyn[2]=int(widgetno)-1        
        Update_Zoombox(True)        
def Zoom_button_release(event):
    if radio_Imgorhis.get()=='image': 
        Update_Zoombox(True)
        
def Zoom_button_press(event):
    if radio_Imgorhis.get()=='image': 
        Update_Zoombox(False)

def every_X(arr,x):
    newlist = []
    for i in range(len(arr)):
        if i % x == 0:
            newlist.append(arr[i])
    return newlist
        
def Crop_button_press(event):
    # save mouse drag start position
    global Crp ; Crp['M']=ImgCropM.get()
    CropStart.append(event.x) ;  CropStart.append(event.y)

def Crop_move_press( event):
    x=5 if event.x<0 else event.x
    y=5 if event.y<0 else event.y
    if event.x > PicDim['W']* Crop_zs: x=PicDim['W'] * Crop_zs-5
    if event.y > PicDim['H']* Crop_zs: y=PicDim['H'] * Crop_zs-5
    ImgCropW.set(int(abs(x-CropStart[0])/Crop_zs)) ;  ImgCropH.set(int(abs(y-CropStart[1])/Crop_zs))
    newx=x if x<CropStart[0] else CropStart[0]        
    newy=y if y<CropStart[1] else CropStart[1] 
    ImgCropX.set(int(newx/Crop_zs)) ;  ImgCropY.set(int(newy/Crop_zs)) 
    for i in range(0,1+1):
        pi=1 if i==0 else 0
        clr='blue' if i==0 else 'yellow'
        CropCanvas[i].delete(CropCanvas[i].find_withtag('CropRec'))        
        CropCanvas[i].create_rectangle(-(pi*Crp['M']* Crop_zs)+CropStart[0], CropStart[1], -(pi*Crp['M']* Crop_zs)+x, y, outline=clr,tags='CropRec')  

def Crop_button_release( event):
    del CropStart[:]
    pass       

def create_widgets(): 
    #Create some room around all the internal frames
    root['padx'] = 6;    root['pady'] = 6
    #Panels  
    panes = PanedWindow(root, orient=HORIZONTAL)  
    panes.pack(fill=BOTH, expand=1)
    Pane1 = ttk.Labelframe(panes, text='Input') ;  Pane2 = ttk.Labelframe(panes, text='Operation')  
    panes.add(Pane1); panes.add(Pane2)
    panes.config(showhandle='false')
    panes.paneconfigure(Pane1, sticky='nsew', width=240)
    
    # Pane 1 >>> 
    ef1 = LabelFrame(Pane1, text="", relief=RIDGE); ef1.grid(row=1, column=1,sticky=W) 
    ef2 = LabelFrame(Pane1, text="", relief=RIDGE); ef2.grid(row=2, column=1,sticky=W) 
    # Pane 1, ef1 >>> 
    # Select Folder and Run Button
    Label(ef1, text='' ).grid(row=0, column=1,columnspan=2,sticky=W)
    Button(ef1 , text = '    Select Directory    ', command =SlctFolder).grid(row =1, column=1,columnspan=2) 
    Button(ef1 , text = '           Update            ', command =UpdateButton).grid(row =2, column=1,columnspan=2)  
    Label(ef1, text='' ).grid(row=3, column=1,columnspan=2,sticky=W)
        
    ttk.Separator( ef1, orient=HORIZONTAL).grid(row=4, column=1,columnspan=2, sticky=E+W)
    Label(ef1, text='Measuring time step (sec): ' ).grid(row=5, column=1,columnspan=2,sticky=W)
    Label(ef1, text='Initial records to skip: ' ).grid(row=7, column=1,columnspan=2,sticky=W)
    Label(ef1, text='Final records to skip: ' ).grid(row=9, column=1,columnspan=2,sticky=W)
    Label(ef1, text='Select every X images, X: ' ).grid(row=11, column=1,columnspan=2,sticky=W)

    Entry(ef1 , textvariable = Mstep,width=5).grid(row = 6, column = 1 ,columnspan=2) ;   Mstep.set(0.5)
    Entry(ef1 , textvariable = SkipI,width=5).grid(row = 8, column = 1 ,columnspan=2) ;   SkipI.set(0)
    Entry(ef1 , textvariable = SkipF,width=5).grid(row = 10, column = 1 ,columnspan=2) ;   SkipF.set(1)    
    Scale(ef1,from_=1, to=30, resolution=1,variable = everyXimage,length=120,orient=HORIZONTAL).grid(row=12, column=1,columnspan=2) 
    everyXimage.set(8)   
    Label(ef1, textvariable=ProcessImgVar,justify=LEFT ).grid(row=13, column=1,columnspan=2,sticky=W)
    ttk.Separator( ef1, orient=HORIZONTAL).grid(row=14, column=1,columnspan=2, sticky=E+W)
    Label(ef1, text='Specimen Diameter (mm) : ' ).grid(row=15, column=1,sticky=W)
    Entry(ef1 , textvariable = SpecDiaVar,width=8).grid(row = 15, column = 2 ) ;   SpecDiaVar.set(6.01)
    ttk.Separator( ef1, orient=HORIZONTAL).grid(row=16, column=1,columnspan=2, sticky=E+W)
    
    Label(ef1, textvariable=TotalImgVar,justify=LEFT ).grid(row=17, column=1,columnspan=2,sticky=W)
    TotalImgVar.set('Total number of records:  ... ')
    Label(ef1, text='Rotation Angle:',justify=LEFT ).grid(row=18, column=1,columnspan=2,sticky=W)
    Label(ef1, textvariable=Rotation_AngleVar,justify=LEFT ).grid(row=19, column=1,columnspan=2)
    Label(ef1, text='pixel per mm:',justify=LEFT ).grid(row=20, column=1,columnspan=2,sticky=W)
    Label(ef1, textvariable=pixel_lenVar,justify=LEFT ).grid(row=21 , column=1,columnspan=2)   
    Rotation_AngleVar.set('...') ; pixel_lenVar.set('...')
    
    # Pane 1, ef2 >>> 
    Label(ef2, textvariable=statustext,justify=LEFT ).grid(row=1, column=1, sticky=W) ; statustext.set('Status: Ready') 
    global progress; progress=ttk.Progressbar(ef2,orient=HORIZONTAL,length=200,mode='determinate') ;progress.grid(row=2, column=1)
    
    ttk.Separator( ef1, orient=HORIZONTAL).grid(row=30, column=1,columnspan=2, sticky=E+W)
    Button(ef1 , text = '            Run            ', command = Run).grid(row =31, column=1,columnspan=2) 

    
    # Panel 2 >>>
    global tb; tb = ttk.Notebook(Pane2)          # Create Tab Control
    CropTab = ttk.Frame(tb) ; global GraphTab ; GraphTab = ttk.Frame(tb)                    # Create tabs    
    ParamTab = ttk.Frame(tb) ;  RunTab = ttk.Frame(tb)                    # Create tabs 
    tb.add(CropTab, text='     Crop    ') ; tb.add(GraphTab, text='    Neck Position    ')     # Add the tab
    tb.add(ParamTab, text='     Sample Images    ') ; tb.add(RunTab, text='    Run     ')     # Add the tab
    tb.pack(expand=1, fill="both")   # Pack to make visible    
     
    # Crop Tab >>>       
    CropPicH=CropCanvas_W*PicDim['H']/PicDim['W']   
    for i in range(0,1+1):
        CropCanvas.append(Canvas(CropTab, bg="lightgray", height=CropPicH, width=CropCanvas_W)) ;  CropCanvas[i].grid(row=1, column=i+1) 
        
    CFrame = LabelFrame(CropTab, text="Crop Size", relief=RIDGE); CFrame.grid(row=2, column=1,sticky=W)  
    Label(CFrame, text='Original Image size:',justify=LEFT ).grid(row=2, column=1,sticky=W)
    Label(CFrame, text='Croped Image Size:',justify=LEFT ).grid(row=3, column=1,sticky=W)
    Label(CFrame, text='Croped Image Location:  ',justify=LEFT ).grid(row=4, column=1,sticky=W)
    Label(CFrame, text='Crop Backward Movement:    ',justify=LEFT ).grid(row=5, column=1,sticky=W)
    Label(CFrame, textvariable=ImgOrigSize ).grid(row=2, column=2,columnspan=2)
    Entry(CFrame , textvariable = ImgCropW,width=6,state='readonly').grid(row = 3, column = 2 ) 
    Entry(CFrame , textvariable = ImgCropH,width=6,state='readonly').grid(row = 3, column = 3 )
    Entry(CFrame , textvariable = ImgCropX,width=6,state='readonly').grid(row = 4, column = 2 ) 
    Entry(CFrame , textvariable = ImgCropY,width=6,state='readonly').grid(row = 4, column = 3 )  
    Entry(CFrame , textvariable = ImgCropM,width=8).grid(row = 5, column = 2 )  
    # Default Crop Values, for only the first load
    ImgCropW.set(Crp['W']) ; ImgCropH.set(Crp['H'])  
    ImgCropX.set(Crp['X']) ; ImgCropY.set(Crp['Y']) 
    ImgCropM.set(Crp['M'])    

    # Graph Tab >>>   
    GraphPicW= (Dim['W']-300) ; GraphPicH=GraphPicW*0.615
    global Graph_CVS ; Graph_CVS=Canvas(GraphTab, bg="lightgray", height=GraphPicH, width=GraphPicW) ;  Graph_CVS.grid(row=1, column=1,sticky=N+W)   
    GFrame = LabelFrame(GraphTab, text="Smoothing", relief=RIDGE); GFrame.grid(row=2, column=1,sticky=W)  
    Label(GFrame, text='Degree of smoothing:',justify=LEFT ).grid(row=1, column=1,sticky=W)    
    Scale(GFrame,from_=0, to=5, resolution=1,variable = SmDegree,length=120,orient=HORIZONTAL).grid(row=2, column=1) ;  SmDegree.set(1) 
    
    # Param Tab >>> 
    for i in range(0,4+1):
        ParamCanvas.append(Canvas(ParamTab, bg="lightgray", height=ParamPic['H'], width=ParamPic['W'])) ;  ParamCanvas[i].grid(row=1, column=i+1,sticky=N+W) 
    
    PFrame0 = LabelFrame(ParamTab, text="", relief=RIDGE); PFrame0.grid(row=2, column=1,columnspan=5,sticky=N+W)
    global Zbox ; Zbox=Canvas(PFrame0, bg="lightgray", height=GraphPicW*0.7/ZboxRatio, width=GraphPicW*0.7) ;  Zbox.grid(row=1, column=1,columnspan=1,sticky=N+W)  
    PFrame = LabelFrame(PFrame0, text="Parameters", relief=RIDGE); PFrame.grid(row=1, column=2,columnspan=1,sticky=N+W)

    Radiobutton(PFrame, text="Sample Images", variable=radio_Imgorhis, value='image', command =ImageOrHist).grid(row=1, column=1,sticky=W)
    Radiobutton(PFrame, text="Grey scale scan", variable=radio_Imgorhis, value='hist',command =ImageOrHist).grid(row=2, column=1,sticky=W)   
    radio_Imgorhis.set('image') 
    Label(PFrame, text='Noise Canceling degree: ',justify=LEFT ).grid(row=4, column=1,sticky=W)  
    Scale(PFrame,from_=0, to=20, resolution=2,variable = BlurDegVar,length=100,orient=HORIZONTAL).grid(row=4, column=2) 
    BlurDegVar.set(4)  
    Label(PFrame, text='Sample scan line position: ',justify=LEFT ).grid(row=5, column=1,sticky=W)   
    Scale(PFrame,from_=0, to=100, resolution=1,variable = Line_PosVar,length=100,orient=HORIZONTAL).grid(row=5, column=2) 
    Line_PosVar.set(50) 
    Label(PFrame, text='Contour line thickness: ',justify=LEFT ).grid(row=6, column=1,sticky=W)   
    Scale(PFrame,from_=1, to=6, resolution=1,variable = Line_thkVar,length=100,orient=HORIZONTAL).grid(row=6, column=2) 
    Line_thkVar.set(4)
    Label(PFrame, text='Zoom Degree: ',justify=LEFT ).grid(row=7, column=1,sticky=W)   
    Scale(PFrame,from_=1, to=5, resolution=1,variable = ZoomVar,length=100, command =Update_Zoomlevel,orient=HORIZONTAL).grid(row=7, column=2) 
    ZoomVar.set(2)
    ttk.Separator( PFrame, orient=HORIZONTAL).grid(row=8, column=1,columnspan=2, sticky=E+W)
    Label(PFrame, text='Threshod factor: ',justify=LEFT ).grid(row=9, column=1,sticky=W)   
    Scale(PFrame,from_=0.6, to=1.2, resolution=0.01,variable = retVar,length=100,orient=HORIZONTAL).grid(row=9, column=2) 
    retVar.set(0.74) #0.74
    Label(PFrame, text='Endpoint Factor: ',justify=LEFT ).grid(row=10, column=1,sticky=W)   
    Scale(PFrame,from_=0.1, to=0.4, resolution=0.02,variable = M1Var,length=100,orient=HORIZONTAL).grid(row=10, column=2) 
    M1Var.set(0.2)
    Label(PFrame, text='Background line Factor: ',justify=LEFT ).grid(row=11, column=1,sticky=W)   
    Scale(PFrame,from_=5, to=30, resolution=1,variable = M2Var,length=100,orient=HORIZONTAL).grid(row=11, column=2) 
    M2Var.set(20)
    
    # Run Tab >>>  
    #aR_W= (Dim['W']-300) ; aR_H=aR_W*0.615
    aR_H=Dim['H']*0.45; aR_W= aR_H/0.615
    #minD_W= aR_W/2.01; minD_H=minD_W*0.615
    minD_W= aR_W/1; minD_H=minD_W*0.615
    global aR_CVS ; aR_CVS=Canvas(RunTab, bg="lightgray", height=aR_H, width=aR_W) ;  aR_CVS.grid(row=1, column=1,sticky=N+W,columnspan=2,rowspan=2) 
    global minD_CVS ; minD_CVS=Canvas(RunTab, bg="lightgray", height=minD_H, width=minD_W) ;  minD_CVS.grid(row=3, column=1,columnspan=2,sticky=N+W) 
    fitU_W= (PaneWidth-aR_W-10)*0.6 ; fitU_H=aR_H/2.02
    global fitU_CVS ; fitU_CVS=Canvas(RunTab, bg="lightgray", height=fitU_H, width=fitU_W) ;  fitU_CVS.grid(row=1, column=3,sticky=N+W) 
    global fitL_CVS ; fitL_CVS=Canvas(RunTab, bg="lightgray", height=fitU_H, width=fitU_W) ;  fitL_CVS.grid(row=2, column=3,sticky=N+W) 
    Con_W= (PaneWidth-aR_W-10)*0.4 ; Con_H=aR_H
    global Con_CVS ; Con_CVS=Canvas(RunTab, bg="lightgray", height=Con_H, width=Con_W) ;  Con_CVS.grid(row=1, column=4,sticky=N+W,rowspan=2) 
    global RunP_CVS ; RunP_CVS=Canvas(RunTab, bg="lightgray", height=minD_H, width=fitU_W+Con_W) ;  RunP_CVS.grid(row=3, column=3,columnspan=2,sticky=N+W) 
    global ConPic; ConPic={'W':Con_W,'H':Con_H,'X0':0 , 'Y0':0 }
    
    #GFrame = LabelFrame(GraphTab, text="Smoothing", relief=RIDGE); GFrame.grid(row=2, column=1,sticky=W)  
    #Label(GFrame, text='Degree of smoothing:',justify=LEFT ).grid(row=1, column=1,sticky=W)    
    #Scale(GFrame,from_=0, to=5, resolution=1,variable = SmDegree,length=120,orient=HORIZONTAL).grid(row=2, column=1) ;  SmDegree.set(1) 


#----------------------------------------------------------------------------------Main BODY
started=False
# GUI Main Window Options    
root = Tk() 
root.title("OMT"); root.resizable(False, False)
Dim={'W':1600,'H':1000}    # Main GUI Size
root.geometry("%dx%d" % (Dim['W'], Dim['H']))
root.geometry("+%d+%d" % ((root.winfo_screenwidth() - Dim['W']) / 2, (root.winfo_screenheight() - Dim['H']) / 2 )) # so that GUI appears in the center of screen

PicDim={'W':4800,'H':3200} # Default dimension value of images, doesn't matter, will be modified later according to the given images      
#Crp={'W':PicDim['W']*0.4,'H':PicDim['H']*0.5 , 'X':PicDim['W']*0.25,'Y':PicDim['H']*0.25 } # Default Crop Values
Crp={'W':3000-800,'H':3800-300 , 'X':800,'Y':300 ,'M':0} # Default Crop Values fornow

PaneWidth =Dim['W']-240 
ParamPic={'W':PaneWidth/5-12,'H':(PaneWidth/5-12)*2,'X0':0 , 'Y0':0 }


TotalImgVar = StringVar()   
ImgCropW = IntVar() ; ImgCropH = IntVar() ; ImgCropX = IntVar() ; ImgCropY = IntVar() ; ImgCropM = IntVar()
Mstep=DoubleVar(); SkipI =IntVar() ; SkipF =IntVar() ;  everyXimage =IntVar() ; ProcessImgVar = StringVar()  
SpecDiaVar =DoubleVar()
ImgOrigSize = StringVar() 
statustext= StringVar() 

CropCanvas=[] ;  CropCanvas_W= PaneWidth/2 -15
ParamCanvas=[] 
SmDegree =IntVar()
BlurDegVar =IntVar() ; Line_PosVar=IntVar() ; Line_thkVar=IntVar() ; radio_Imgorhis =StringVar() ; ZoomVar=DoubleVar()
Rotation_AngleVar = StringVar() ; pixel_lenVar = StringVar()
retVar=DoubleVar() ; M1Var=DoubleVar() ; M2Var=IntVar()
RNO=0
ZboxRatio=2.4 
create_widgets()# ; root.update_idletasks()
CropStart = [] ; Zoom_xyn = [0,0,0]
Crop_zs=1 ; Param_zs=1 ; ZoomBox_zs=1
rotate_angle=0
pixel_diameter=0 ; pixel_per_mm =0
iW=[] ; iH=[] ; iWLen=0 ; iHLen=0
NeckIndex=0
Pivot=[] ; iWR=[]
Centerline=[] # For testing rotation
# global values used in curve fitting part
Hleft,Hright=0,0
Nm_x , Nm_y1, Nm_y2 , Ncoarse_dmin=0,0,0,0
medfiltpara = 3 #default     
##files['png']=[] ; txt_list=[]
for i in range(1,1+1):
    CropCanvas[i].bind("<ButtonPress-1>", Crop_button_press)
    CropCanvas[i].bind("<B1-Motion>", Crop_move_press)
    CropCanvas[i].bind("<ButtonRelease-1>", Crop_button_release)
for i in range(0,4+1): ParamCanvas[i].bind("<ButtonPress-1>", Param_button_press)
Zbox.bind("<ButtonPress-1>", Zoom_button_press)
Zbox.bind("<ButtonRelease-1>", Zoom_button_release)

#Creating figures and subplot
f1 =plt.figure(figsize=(13,8)) ; ax1 = f1.add_subplot(111)
f2 =plt.figure(figsize=(4,8)) ; ax2 = f2.add_subplot(111)
f3 =plt.figure(figsize=(13,8)) ; ax3 = f3.add_subplot(111)
f4 =plt.figure(figsize=(9,6)) ; ax4 = f4.add_subplot(111)
f5 =plt.figure(figsize=(6,4)) ; ax5 = f5.add_subplot(111)
#-------- Start
root.update()
statustext.set('Status: \nSelect directory including data') 
started=True
print('>>>>>>>>>>>>>  Run Duration: %.2f sec'  %(time.time() - starttime))
root.mainloop()
