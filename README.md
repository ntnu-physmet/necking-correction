# Necking correction

![matplotlib screencast](necking.gif)

## Contributors
Feng Lu (Phd work), Hassan Moradi Asadkandi (programming), Ida Lægreid Andersen (MSc work), Tomas Manik (co-supervisor), Bjørn Holmedal (supervisor). All at the Norwegian University of Science and Technology (NTNU), the physical metallurgy group. https://www.ntnu.edu/ima/research/physical-metallurgy#/view/about.

## About
The software returns the contours of axisymetric tensile tests and finds the minmum radius and the radius of curvature  during necking. The input is a sequence of images recorded during the test. It has a user-friendly GUI. 

The precise determination of the stress-strain relationship up to large strains is one of the challenges for accurate modeling of several forming operations involving large strains. A tensile test is a fundamental mechanical test that is used to determine the stress-strain curve of a material. It is challenging to determine the stress-strain curve up to fracture as the neck development complicates the determination of the tensile stress. However, by using some optical measurement technique, e.g. a high-resolution camera, necking contours of the tensile sample during the straining can be captured. A dedicated contour-tracking algorithm needs to be used to cope with surface roughness that develops during testing and makes precise determination of specimen contours very challenging. The radius of the minimal cross-sectional area and the radius of curvature are used for analytical necking corrections of the stress-strain curve. 

We have developed an image processing algorithm for performing necking correction to obtain equivalent stress-strain curves up to fracture for a ductile material. The experimental setup requires a high-resolution camera with synchronized acquisition of force and images during a tensile test of an axisymmetric specimen with good light conditions. 

## Requirements
The following packages need to be installed:


## Usage

Start by cloning the code as
```bash
git clone https://gitlab.com/ntnu-physmet/necking-correction.git
```
Then run as

```bash
python necking_correction.py
```

## Example
For getting started, see the [instruction video](https://www.youtube.com/watch?v=uZTVbMz7VgI&feature=youtu.be). 
[![Watch the video](https://gitlab.com/ntnu-physmet/necking-correction/-/raw/master/Screenshot.PNG)](https://youtu.be/uZTVbMz7VgI)


